"use strict";
exports.__esModule = true;
var animal = /** @class */ (function () {
    function animal(nome_animal) {
        this.nome_animal = nome_animal;
    }
    animal.prototype.pet = function () {
        return this.nome_animal;
    };
    return animal;
}());
exports.animal = animal;
