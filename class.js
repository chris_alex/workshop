"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var animal_1 = require("./animal");
var User = /** @class */ (function (_super) {
    __extends(User, _super);
    function User() {
        return _super.call(this, 'Cachorro') || this;
    }
    User.prototype.createUser = function (nome) {
        return this.usuario = {
            nome: nome,
            cpf: '9999999',
            nacionalidade: 'Brazil',
            nasci: 'hoje'
        };
    };
    return User;
}(animal_1.animal));
exports.User = User;
var usuario = new User();
console.log(usuario.createUser('Christian'), usuario.pet());
