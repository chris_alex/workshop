"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
exports.__esModule = true;
var Spider = /** @class */ (function () {
    function Spider(spider_name) {
        this.spider_name = spider_name;
    }
    Spider.prototype.createSpider = function () {
        var hero = this.hero;
        hero = {
            power: ['Força,Escalada, Spider-scese, cooles heros in the fuking universe'],
            age: 17,
            name: this.spider_name
        };
        return __assign({}, hero);
    };
    return Spider;
}());
exports.Spider = Spider;
var myHero = new Spider('Christian');
console.log(myHero.createSpider(), myHero.spider_name);
